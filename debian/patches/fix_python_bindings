Description: Fix build system for Python bindings.
 The build system in capstone for the Python bindings works by installing
 the library with the headers, and then building the bindings.  Change that
 to allow building them in the same run we create the library, and search
 for the versioned library instead of the .so one, removing the dependency
 on libcapstone-dev.
Author: David Martínez Moreno <ender@debian.org>
Forwarded: no
Last-Update: 2014-05-05

--- a/bindings/python/setup_cython.py
+++ b/bindings/python/setup_cython.py
@@ -6,25 +6,27 @@ from Cython.Distutils import build_ext
 VERSION = '3.0'
 
 compile_args = ['-O3', '-fomit-frame-pointer']
+include_dirs = ['../../../include']
+library_dirs = ['../../../']
 
 ext_modules = [
-    Extension("capstone.ccapstone", ["pyx/ccapstone.pyx"], libraries=["capstone"], extra_compile_args=compile_args),
-    Extension("capstone.arm", ["pyx/arm.pyx"], extra_compile_args=compile_args),
-    Extension("capstone.arm_const", ["pyx/arm_const.pyx"], extra_compile_args=compile_args),
-    Extension("capstone.arm64", ["pyx/arm64.pyx"], extra_compile_args=compile_args),
-    Extension("capstone.arm64_const", ["pyx/arm64_const.pyx"], extra_compile_args=compile_args),
-    Extension("capstone.mips", ["pyx/mips.pyx"], extra_compile_args=compile_args),
-    Extension("capstone.mips_const", ["pyx/mips_const.pyx"], extra_compile_args=compile_args),
-    Extension("capstone.ppc", ["pyx/ppc.pyx"], extra_compile_args=compile_args),
-    Extension("capstone.ppc_const", ["pyx/ppc_const.pyx"], extra_compile_args=compile_args),
-    Extension("capstone.x86", ["pyx/x86.pyx"], extra_compile_args=compile_args),
-    Extension("capstone.x86_const", ["pyx/x86_const.pyx"], extra_compile_args=compile_args),
-    Extension("capstone.sparc", ["pyx/sparc.pyx"], extra_compile_args=compile_args),
-    Extension("capstone.sparc_const", ["pyx/sparc_const.pyx"], extra_compile_args=compile_args),
-    Extension("capstone.systemz", ["pyx/systemz.pyx"], extra_compile_args=compile_args),
-    Extension("capstone.sysz_const", ["pyx/sysz_const.pyx"], extra_compile_args=compile_args),
-    Extension("capstone.xcore", ["pyx/xcore.pyx"], extra_compile_args=compile_args),
-    Extension("capstone.xcore_const", ["pyx/xcore_const.pyx"], extra_compile_args=compile_args)
+    Extension("capstone.ccapstone", ["pyx/ccapstone.pyx"], libraries=["capstone"], extra_compile_args=compile_args, include_dirs=include_dirs, library_dirs=library_dirs),
+    Extension("capstone.arm", ["pyx/arm.pyx"], extra_compile_args=compile_args, include_dirs=include_dirs, library_dirs=library_dirs),
+    Extension("capstone.arm_const", ["pyx/arm_const.pyx"], extra_compile_args=compile_args, include_dirs=include_dirs, library_dirs=library_dirs),
+    Extension("capstone.arm64", ["pyx/arm64.pyx"], extra_compile_args=compile_args, include_dirs=include_dirs, library_dirs=library_dirs),
+    Extension("capstone.arm64_const", ["pyx/arm64_const.pyx"], extra_compile_args=compile_args, include_dirs=include_dirs, library_dirs=library_dirs),
+    Extension("capstone.mips", ["pyx/mips.pyx"], extra_compile_args=compile_args, include_dirs=include_dirs, library_dirs=library_dirs),
+    Extension("capstone.mips_const", ["pyx/mips_const.pyx"], extra_compile_args=compile_args, include_dirs=include_dirs, library_dirs=library_dirs),
+    Extension("capstone.ppc", ["pyx/ppc.pyx"], extra_compile_args=compile_args, include_dirs=include_dirs, library_dirs=library_dirs),
+    Extension("capstone.ppc_const", ["pyx/ppc_const.pyx"], extra_compile_args=compile_args, include_dirs=include_dirs, library_dirs=library_dirs),
+    Extension("capstone.x86", ["pyx/x86.pyx"], extra_compile_args=compile_args, include_dirs=include_dirs, library_dirs=library_dirs),
+    Extension("capstone.x86_const", ["pyx/x86_const.pyx"], extra_compile_args=compile_args, include_dirs=include_dirs, library_dirs=library_dirs),
+    Extension("capstone.sparc", ["pyx/sparc.pyx"], extra_compile_args=compile_args, include_dirs=include_dirs, library_dirs=library_dirs),
+    Extension("capstone.sparc_const", ["pyx/sparc_const.pyx"], extra_compile_args=compile_args, include_dirs=include_dirs, library_dirs=library_dirs),
+    Extension("capstone.systemz", ["pyx/systemz.pyx"], extra_compile_args=compile_args, include_dirs=include_dirs, library_dirs=library_dirs),
+    Extension("capstone.sysz_const", ["pyx/sysz_const.pyx"], extra_compile_args=compile_args, include_dirs=include_dirs, library_dirs=library_dirs),
+    Extension("capstone.xcore", ["pyx/xcore.pyx"], extra_compile_args=compile_args, include_dirs=include_dirs, library_dirs=library_dirs),
+    Extension("capstone.xcore_const", ["pyx/xcore_const.pyx"], extra_compile_args=compile_args, include_dirs=include_dirs, library_dirs=library_dirs)
 ]
 
 # clean package directory first
--- a/bindings/python/Makefile
+++ b/bindings/python/Makefile
@@ -42,7 +42,7 @@ install_cython:
 	cp capstone/xcore_const.py $(OBJDIR)/pyx/xcore_const.pyx
 	cd $(OBJDIR) && python setup_cython.py build -b ./tmp install --home=$(OBJDIR)
 	mv $(OBJDIR)/build/lib/python/capstone/* capstone
-	cd $(OBJDIR) && python setup_cython.py build -b ./tmp install
+	cd $(OBJDIR) && python setup_cython.py build -b ./tmp install $(OPTS)
 
 clean:
 	rm -rf $(OBJDIR)
